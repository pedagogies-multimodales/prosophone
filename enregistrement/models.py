from django.db import models

class Recording(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True, auto_now=False)
    auteur = models.CharField(default='anonymous', max_length=100)
    audio = models.TextField(default='')
    test = models.CharField(default='0', max_length=10)
    session = models.CharField(default='0', max_length=10)
    questionnaire = models.CharField(default='0', max_length=10)