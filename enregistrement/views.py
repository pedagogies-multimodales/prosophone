import enregistrement
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.views.decorators.clickjacking import xframe_options_exempt
from enregistrement.models import Recording
import json, base64, wave, datetime

@xframe_options_exempt
def main(request):
    user = request.GET.get('username', None)
    session = request.GET.get('session', None)
    questionnaire = request.GET.get('questionnaire', None)
    data = { 
        'user':user, 
        'session':session, 
        'questionnaire':questionnaire
    }
    return render(request, 'enregistrement/rec.html', data)

@xframe_options_exempt
def addRec(request):
    colis = json.loads(request.body)
    # print("Colis reçu !", colis)
    # new_item = Recording(content = request.POST['content'])
    # new_item.save()
    # messages.success(request, f"L'enregistrement a bien été sauvegardé !")
    
    fname = f"../media/prosophone/enregistrements/session{colis['session']}-item{colis['questionnaire']}-user{colis['user']}_{ datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S') }.wav" 

    with wave.open(fname,'w') as outf:
        outf.setnchannels(2)
        outf.setsampwidth(2)
        outf.setframerate(colis['sampleRate'])
        outf.writeframes(base64.b64decode(colis['audio']))

    data= {
        'saved': True
    }
    return JsonResponse(data)



def delRec(request, rec_id):
    item_to_delete = Recording.objects.get(id=rec_id)
    item_to_delete.delete()
    messages.success(request, f"L'enregistrement a été supprimé !")
    return render(request, 'enregistrement/rec.html')